# Word Cloud ABC

Word Cloud ABC is a preset for the Moodle activity database.

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

## Language Support

The preset is available in German. 

## Description

Students enter terms into an ABC list. A word cloud is generated from this.

## License

CC BY-NC-SA 4.0 DE
Word cloud layout by Jason Davies, http://www.jasondavies.com/word-cloud/
Algorithm due to Jonathan Feinberg, http://static.mrfeinberg.com/bv_ch03.pdf

## Screenshots

<img width="400" alt="single view" src="/screenshots/einzelansicht.png">

<img width="400" alt="list view" src="/screenshots/listenansicht.png">
